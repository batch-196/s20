let number = prompt("Please enter the First Number");
number = parseInt(number);


console.log("The number you provided is " + number);
for (number; number >= 0; number--) {
    if (number <= 50) {
        break;
    }

    if (number % 10 === 0) {
        console.log("The number is being skipped");
        continue;
    }

    if (number % 5 === 0) {
        console.log(number);
        continue;
    }
}


function multiplyBy5(num) {
    product = num * 5;
    return product;
}

for (let x = 1; x <= 10; x++) {
    let result = multiplyBy5(x);
    console.log("5 x " + x + " = " + result);
}